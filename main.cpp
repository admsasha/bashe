#include <QtGui>
#include <QApplication>

#include "bashe.h"

int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF8"));

    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

    bashe form1;
    form1.show();

    return app.exec();
}
