#ifndef MBUTTON_H
#define MBUTTON_H

#include <QWidget>
#include <QLabel>


class MButton : public QWidget{
    Q_OBJECT
    public:
        MButton(QWidget *parent = 0);
        void setPicEvent(QString picLeave,QString picEnter);
        void setText(QString text);
        void setFont(QFont font);

    private:
        void setPicture(QString picture);

        QLabel *pic;
        QLabel *txt;
        QString picLeave;
        QString picEnter;



    protected:
        bool eventFilter(QObject*, QEvent *);
        void resizeEvent ( QResizeEvent * event );

    signals:
        void clicked();

};

#endif // MBUTTON_H
