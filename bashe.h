#ifndef BASHE_H
#define BASHE_H

#include <QWidget>
#include <QLabel>
#include "MButton.h"

class bashe : public QWidget{
    Q_OBJECT
    public:
        bashe();

    private:
        void turn(int count);
        void botTurn();

        QPixmap pic;
        MButton *button_v1;
        MButton *button_v2;
        MButton *button_v3;
        MButton *button_exit;

        MButton *button_new;
        MButton *button_info;
        MButton *button_exit2;

        MButton *buttonAI;        // изменение сложности
        MButton *buttonPlayer;    // порядок хода

        MButton *buttonInfo;      // строка Info


        QLabel *spichki[16];    // Массив спичек
        QLabel *msg;            // Строка сообщений
        QLabel *head;           // Шапка
        QLabel *copy;           // строка с копирайтом
        QLabel *copyD;          // строка с датой релиза

        int spichks;    // колво оставшихся спичек
        int isGame;     // Началась ли игра
        int hard;       // Сложность (0-сложный, 1-легкий)
        int first;      // Кто первый ходит (0-компьютер, 1-игрок)

        int MouseStartPosX;
        int MouseStartPosY;


    protected:
	void resizeEvent(QResizeEvent */*e*/);
	void paintEvent(QPaintEvent *e);
        bool eventFilter(QObject*, QEvent *);


    private slots:
        void humanTurn1();
        void humanTurn2();
        void humanTurn3();
        void newGames();
        void changeHard();
        void changeFirst();
        void showInfo();
        void hideInfo();


};

#endif // BASHE_H
