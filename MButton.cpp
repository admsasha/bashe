#include "MButton.h"

#include <QtGui>
#include <QPixmap>
#include <QImage>

MButton::MButton(QWidget *parent) : QWidget(parent){
    pic = new QLabel("",this);
    pic->setMouseTracking(true);
    pic->installEventFilter(this);

    txt = new QLabel("",this);
    txt->setAlignment(Qt::AlignCenter);

    txt->setMouseTracking(true);
    txt->installEventFilter(this);

}

void MButton::resizeEvent ( QResizeEvent * event ) {
    pic->setFixedSize(event->size().width(),event->size().height());
    txt->setFixedSize(event->size().width(),event->size().height());
}


void MButton::setText(QString text){
    txt->setText(text);
}

void MButton::setFont(QFont font){
    txt->setFont(font);
}


void MButton::setPicEvent(QString picleave,QString picenter){
    picLeave=picleave;
    picEnter=picenter;

    setPicture(picLeave);

}


bool MButton::eventFilter(QObject *o, QEvent *e){

    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(e);

    if (e->type() == QEvent::Leave){
        if (o == txt and picLeave.length()>0){
            setPicture(picLeave);
            return 1;
        }
    }

    if (e->type() ==   QEvent::Enter){
        if (o == txt and picEnter.length()>0){
            setPicture(picEnter);
            return 1;
        }
    }

    if (e->type() == QEvent::MouseButtonRelease){
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(e);
        if (mouseEvent->button() == Qt::LeftButton){
            if (o == txt){
                emit clicked();
                return 1;
            }
        }
    }

    return QWidget::eventFilter(o, e);
}


void MButton::setPicture(QString picture){
    QImage im;
    QPixmap pixmap;

    pic->setPixmap(QPixmap(picture));

    im.load(picture);
    pixmap= QPixmap::fromImage(im);
    pic->setPixmap(pixmap);
}
