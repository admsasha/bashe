#include <QtGui>
#include <QApplication>
#include <QDesktopWidget>

#include "bashe.h"

bashe::bashe(){
    pic.load(":/images/backgroud.png");
    move(300,300);
    setFixedSize(624,482);

    QRect desk(QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this)));
    move((desk.width() - width()) / 2, (desk.height() - height()) / 2);

    spichks=15;
    isGame=0;
    hard=0;
    first=0;


    QFont fontCopy;
    fontCopy.setPointSize(8);


    QPalette palette;
    QFont font2;

    QBrush brush(QColor(255, 255, 255, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::WindowText, brush);

    font2.setBold(true);
    font2.setPointSize(10);
    font2.setWeight(75);


    head = new QLabel(this);
    head->setGeometry(0,2,624,20);
    head->setText("   Баше  v1.0");
    head->setFont(font2);
    head->setPalette(palette);
    head->installEventFilter(this);

    copy = new QLabel(this);
    copy->setGeometry(19,460,305,11);
    copy->setFont(fontCopy);
    copy->setText("Copyright DanSoft. All rights reserved.");

    copyD = new QLabel(this);
    copyD->setGeometry(505,460,100,11);
    copyD->setFont(fontCopy);
    copyD->setAlignment(Qt::AlignRight);
    copyD->setText("18.04.2010");


    button_v1 = new MButton(this);
    button_v1->setPicEvent(":/images/v1.png",":/images/v1_sel.png");
    button_v1->setGeometry(210,190,65,40);

    button_v2 = new MButton(this);
    button_v2->setPicEvent(":/images/v2.png",":/images/v2_sel.png");
    button_v2->setGeometry(280,190,65,40);

    button_v3 = new MButton(this);
    button_v3->setPicEvent(":/images/v3.png",":/images/v3_sel.png");
    button_v3->setGeometry(350,190,65,40);

    connect(button_v1,SIGNAL(clicked()),this,SLOT(humanTurn1()));
    connect(button_v2,SIGNAL(clicked()),this,SLOT(humanTurn2()));
    connect(button_v3,SIGNAL(clicked()),this,SLOT(humanTurn3()));




    QFont font;
    font.setBold(true);
    font.setPointSize(12);
    font.setWeight(75);


    button_new = new MButton(this);
    button_new->setPicEvent(":/images/btn2_blank.png",":/images/btn2_blank_sel.png");
    button_new->setText("Новая игра");
    button_new->setFont(font);
    button_new->setGeometry(220,36,188,24);
    connect(button_new,SIGNAL(clicked()),this,SLOT(newGames()));


    button_info = new MButton(this);
    button_info->setPicEvent(":/images/btn2_blank.png",":/images/btn2_blank_sel.png");
    button_info->setText("Помощь");
    button_info->setFont(font);
    button_info->setGeometry(220,70,188,24);
    connect(button_info,SIGNAL(clicked()),this,SLOT(showInfo()));


    button_exit2 = new MButton(this);
    button_exit2->setPicEvent(":/images/btn2_blank.png",":/images/btn2_blank_sel.png");
    button_exit2->setText("Выход");
    button_exit2->setFont(font);
    button_exit2->setGeometry(220,104,188,24);
    connect(button_exit2,SIGNAL(clicked()),this,SLOT(close()));

    buttonAI = new MButton(this);
    buttonAI->setText("Компьютер (сложный)");
    buttonAI->setGeometry(8,29,184,15);
    connect(buttonAI,SIGNAL(clicked()),this,SLOT(changeHard()));


    buttonPlayer = new MButton(this);
    buttonPlayer->setText("Игрок (ходит первый)");
    buttonPlayer->setGeometry(431,29,184,15);
    connect(buttonPlayer,SIGNAL(clicked()),this,SLOT(changeFirst()));


    for (int i=1;i<16;i++){
        spichki[i] = new QLabel(this);
        spichki[i]->setPixmap(QPixmap(":/images/spichka.png"));
        spichki[i]->move(5+(37*i),260);
    }

    msg = new QLabel(this);
    msg->setGeometry(210,150,207,26);
    msg->setAlignment(Qt::AlignCenter);
    msg->setText("Нажмите на \"Новая игра\"");


    buttonInfo = new MButton(this);
    buttonInfo->setGeometry(19,249,584,209);
    buttonInfo->setPicEvent(":/images/info.png",":/images/info.png");
    buttonInfo->setText(
        "игра \"Баше\" \n"
        "\n Правила: \nНа игровом поле лежат 15 спичек. \n Играющие поочередно берут от 1 до 3 штук. \n Выигрывает тот, кто возьмет последнии спички. \n"
        "\n Чтобы изменить сложность игры, нажмите \"Компьютер (сложный)\" \n"
        "   Чтобы изменить порядок хода, нажмите \"Игрок (ходит первый)\" \n"
        "\n Что бы начать игру, нажмите \"Новая игра\" \n"
        "\n Чтобы закрыть это окно, просто щелкните на нем."

    );
    buttonInfo->hide();
    connect(buttonInfo,SIGNAL(clicked()),this,SLOT(hideInfo()));


    button_exit = new MButton(this);
    button_exit->setPicEvent(":/images/exit.png",":/images/exit.png");
    button_exit->setGeometry(600,6,12,12);
    connect(button_exit,SIGNAL(clicked()),this,SLOT(close()));

}


void bashe::resizeEvent(QResizeEvent */*e*/){
    setMask(pic.mask());
}
        
void bashe::paintEvent(QPaintEvent *e){
    QRegion r1(pic.rect());
    QPainter painter(this);
            
    painter.setClipRegion(r1);
    painter.drawPixmap(pic.rect(), pic);
    
}


void bashe::newGames(){
    spichks=15;
    for (int i=1;i<16;i++){
        spichki[i]->show();
    }
    isGame=1;

    if (first==0){
        msg->setText("Ходите, ваш ход.");
    }else{
        botTurn();
    }

}

void bashe::humanTurn1(){
    if (isGame==0) return;
    turn(1);
    if (spichks==0){
        msg->setText("Вы выиграли ! \n Нажмите \"Новая игра\"");
        isGame=0;
        return;
    }
    botTurn();
}

void bashe::humanTurn2(){
    if (isGame==0) return;
    turn(2);
    if (spichks==0){
        msg->setText("Вы выиграли ! \n Нажмите \"Новая игра\"");
        isGame=0;
        return;
    }
    botTurn();
}
void bashe::humanTurn3(){
    if (isGame==0) return;
    turn(3);
    if (spichks==0){
        msg->setText("Вы выиграли ! \n Нажмите \"Новая игра\"");
        isGame=0;
        return;
    }
    botTurn();
}


void bashe::botTurn(){
    QString str;
    int count=3;

    if (hard==0){
        count = spichks- (4*int(spichks/4));
    }else{
        count= 1+(qrand()%3);
    }

    if (count==0 or count>3) count=3;
    turn(count);

    str.setNum(count,10);
    msg->setText("Компьютер взял "+str+" спичек \n Ходите, ваш ход.");

    if (spichks==0){
        msg->setText("Вы проиграли ! \n Нажмите \"Новая игра\"");
        isGame=0;
    }

}


void bashe::turn(int count){
    if (count > spichks) count=spichks;

    for (int i=0;i<count;i++){
        spichki[(16-spichks)+i]->hide();
    }

    spichks=spichks-count;
}

void bashe::changeHard(){
    if (isGame==1 and spichks<15){
        msg->setText("Вы не можете изменять \n сложность во время игры");
        return;
    }

    if (hard==0){
        buttonAI->setText("Компьютер (легкий)");
        hard=1;
    }else{
        buttonAI->setText("Компьютер (сложный)");
        hard=0;
    }
}

void bashe::changeFirst(){
    if (first==0){
        buttonPlayer->setText("Игрок (ходит вторым)");
        first=1;
    }else{
        buttonPlayer->setText("Игрок (ходит первый)");
        first=0;
    }
}


bool bashe::eventFilter(QObject *o, QEvent *e){
    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(e);

    if (e->type() == QEvent::MouseMove){
        if (o == head){
    	    if (MouseStartPosX==0 or MouseStartPosY==0) return QWidget::eventFilter(o, e);
        
	    this->move((this->x())+(mouseEvent->globalX()-MouseStartPosX),(this->y())+(mouseEvent->globalY()-MouseStartPosY));
	    MouseStartPosX=mouseEvent->globalX();
	    MouseStartPosY=mouseEvent->globalY();
	}
    }

    if (e->type() == QEvent::MouseButtonPress) {
        if (mouseEvent->button() == Qt::LeftButton){
            if (o == head){
		MouseStartPosX=mouseEvent->globalX();
		MouseStartPosY=mouseEvent->globalY();
    	    }
        
	}
    }

    return QWidget::eventFilter(o, e);
}


void bashe::showInfo(){
    buttonInfo->show();
}


void bashe::hideInfo(){
    buttonInfo->hide();
}
